from turtle import Turtle
from settings import SCREEN_HEIGHT

class Ball(Turtle):
    def __init__(self, shape: str = "circle", undobuffersize: int = 1000, visible: bool = True) -> None:
        super().__init__(shape, undobuffersize, visible)
        self.penup()
        self.color("white")
        self.left(45)
        self.shapesize(0.5,0.5)
        self.x_direction = "right"
        self.y_direction = "top"
        
    def move(self, speed):
        self.forward(3 + speed)

    def is_in_field(self):
        # bottom border
        if self.ycor() < (-(SCREEN_HEIGHT / 2)):
            if self.x_direction == "right":
                self.left(90)
            else:
                self.right(90)
            self.change_y_direction()

        # upper border
        if self.ycor() > ((SCREEN_HEIGHT / 2)):
            if self.x_direction == "right":
                self.right(90)
            else:
                self.left(90)
            self.change_y_direction()
    
    def change_x_direction(self):
        if self.x_direction == "right":
            self.x_direction = "left"
        elif self.x_direction == "left":
            self.x_direction = "right"

    def change_y_direction(self):
        if self.y_direction == "bottom":
            self.y_direction = "top"
        elif self.y_direction == "top":
            self.y_direction = "bottom"