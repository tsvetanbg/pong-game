from turtle import Screen, Turtle
from settings import SCREEN_OFFSET

def _create_border(y:int):
    a = Turtle()
    a.hideturtle()
    a.color("white")
    a.penup()
    a.goto(-2, y / 2)
    a.pensize(5)
    a.pendown()
    a.goto(-2, -(y / 2))

def create_screen(width: int, height: int):
    new_screen = Screen()
    new_screen.setup(width + SCREEN_OFFSET, height + SCREEN_OFFSET)
    new_screen.screensize(width, height)
    new_screen.bgcolor("black")
    new_screen.tracer(0)
    new_screen.listen()

    _create_border(height + SCREEN_OFFSET)

    return new_screen

def add_controls(screen, player):
    screen.onkeypress(key="Up", fun=player.move_up)
    screen.onkeypress(key="Down", fun=player.move_down)



    