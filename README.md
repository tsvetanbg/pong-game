# Pong-game

This is try to reproduce the famuos Pong game.

The game will be reproduced with Python and Turtle package.

## Main tasks

# Main screen
Main screen will initiate the main screen information - the window settings and the canvas.

# User and CPU paddles
The game must have 2 paddles - one controled by the user - the other one controled by the PC.

# Ball
The ball should move from left to right, bouncing from the upper and bottom border and from the paddles.
The ball should not bounce from left and right borders. 
The ball must increase its speed when colide with paddle. Max speed must be added.

# Scoreboard
The game must keep scoreboard and announce the winner after 10 sets.