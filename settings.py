# screen setings
SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 600

# SCREEN_OFFSET is added to remove the scrollbars, when windows is seted up. 
# Screen = the whole window that OS is creating, incuding scrollbars, 
# Canvas = is the inner, visible or not visible part of the window where all action is happening.
SCREEN_OFFSET = 20

# player settings
PLAYER_UPPER_OFFSET = 30
PLAYER_BOTTOM_OFFSET = 40
PLAYER_PADDLE_SPEED = 12

# paddle settings
PADDLE_X_POSITION = (SCREEN_WIDTH / 2) - 20

# game settings
GAME_IS_ON = True
GAME_FRAMES = 0.01 #