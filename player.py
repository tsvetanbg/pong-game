from paddle import Paddle
from settings import (PLAYER_UPPER_OFFSET as UPPER_OFFSET,
                      PLAYER_BOTTOM_OFFSET as BOTTOM_OFFSET,
                      PLAYER_PADDLE_SPEED as PADDLE_SPEED)

class Player(Paddle):
    def __init__(self, screen_height: int, player: bool | None = None, shape: str = "square", undobuffersize: int = 1000, visible: bool = True) -> None:
        super().__init__(screen_height, player, shape, undobuffersize, visible)

    def move_up(self):
        
        self.goto(self.xcor(), self.ycor() + PADDLE_SPEED + self.p_speed)
        
        if self.ycor() > self.screen_height / 2 - UPPER_OFFSET:
            self.undo()

    def move_down(self):
        
        self.goto(self.xcor(), self.ycor() - PADDLE_SPEED + self.p_speed)

        if self.ycor() < - (self.screen_height / 2) + BOTTOM_OFFSET:
            self.undo()


    