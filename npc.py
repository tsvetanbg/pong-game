from paddle import Paddle
from settings import PLAYER_PADDLE_SPEED as PADDLE_SPEED
class Npc(Paddle):
    def __init__(self, screen_height: int, player: bool | None = None, shape: str = "square", undobuffersize: int = 1000, visible: bool = True) -> None:
        super().__init__(screen_height, player, shape, undobuffersize, visible)
        
    
    def move_up(self):
        self.goto(self.xcor(), self.ycor() + PADDLE_SPEED * 0.75)

    def move_down(self):
        self.goto(self.xcor(), self.ycor() - PADDLE_SPEED * 0.75)
