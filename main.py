from screen import create_screen, add_controls
from player import Player
from npc import Npc
from ball import Ball
import time
from settings import (SCREEN_WIDTH,
                      SCREEN_HEIGHT,
                      GAME_IS_ON,
                      GAME_FRAMES,
                      PLAYER_PADDLE_SPEED as PADDLE_SPEED)

screen = create_screen(SCREEN_WIDTH, SCREEN_HEIGHT)
p1 = Player(SCREEN_HEIGHT, player = True)
npc = Npc(SCREEN_HEIGHT)
add_controls(screen, p1)
ball = Ball()

speed = 1

while GAME_IS_ON:
    screen.update()
    time.sleep(GAME_FRAMES)
    
    ball.move(speed)
    ball.is_in_field()

    # player collision
    if ball.xcor() + 10 > p1.xcor():
        if ball.ycor() <= p1.ycor() + 50 or\
           ball.ycor() <= p1.ycor() - 50:

            if ball.y_direction == "top":
                ball.left(90)
            else:
                ball.right(90)        
            ball.change_x_direction()

    #npc collision
    if ball.xcor() - 10 < npc.xcor():

        if ball.ycor() <= npc.ycor() + 50 or\
           ball.ycor() <= npc.ycor() - 50:       

            if ball.y_direction == "bottom":
                ball.left(90)
            else:
                ball.right(90)
            ball.change_x_direction()

    # check if ball is behind player's or cpu paddle. Game over.
    if ball.xcor() > p1.xcor() + 15 or ball.xcor() < npc.xcor() - 15:
        break

    if ball.ycor() > npc.ycor():
        npc.move_up()
    
    if ball.ycor() < npc.ycor():
        npc.move_down()

screen.exitonclick()