from turtle import Turtle
from settings import PADDLE_X_POSITION

class Paddle(Turtle):
    def __init__(self, screen_height: int, player: bool | None = None , shape: str = "square", undobuffersize: int = 1000, visible: bool = True) -> None:
        super().__init__(shape, undobuffersize, visible)
        self.screen_height = screen_height
        self.turtlesize(stretch_wid=5,stretch_len=0.5 )
        self.penup()
        self.color("white")
        
        if player:
            self.goto(PADDLE_X_POSITION,0)
        else:
            self.goto(-PADDLE_X_POSITION, 0)


